﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    public MovingPath Path;
    public float speed = 1f;
    public float maxDist = 0.1f;
    public float timeSpeed = 0.01f;
    public float freezeTime = 5;

    public bool isStarted = false; 

    public IEnumerator<Transform> pointPath;

    private List<GameObject> allPoints = new List<GameObject>();

    //void Start()
    //{
    //    if(Path == null)
    //    {
    //        return;
    //    }

    //    pointPath = Path.GetNextPoint();

    //    pointPath.MoveNext();

    //    if (pointPath.Current == null)
    //    {
    //        return;
    //    }

    //    transform.position = pointPath.Current.position;
    //}


    void Update()
    {
        if (pointPath == null || pointPath.Current == null)
        {
            return;
        }

        transform.position = Vector3.Lerp(transform.position, pointPath.Current.position, Time.deltaTime * speed);

        var distanceSquare = (transform.position - pointPath.Current.position).sqrMagnitude;

        if (distanceSquare < maxDist * maxDist)
        {
            pointPath.MoveNext();
            
        }

        if (Path.isTest)
        {
            print("ssasad");
        }

    }

    public void StartMoving()
    {
        //if (isStarted)
        //{
        //    if (Path == null)
        //    {
        //        return;
        //    }

        //    pointPath = Path.GetNextPoint();

        //    pointPath.MoveNext();

        //    if (pointPath.Current == null)
        //    {
        //        return;
        //    }

        //    transform.position = pointPath.Current.position;
        //}

        StartCoroutine(FreezeTime());
    }

    public IEnumerator FreezeTime()
    {
        if (!isStarted)
        {
            allPoints.AddRange(GameObject.FindGameObjectsWithTag("Empty"));
            foreach(GameObject point in allPoints)
            {
                point.tag = "OffDrag";
                if(point.GetComponent<MovingDragPoints>() != null)
                point.GetComponent<MovingDragPoints>().Speed = point.GetComponent<MovingDragPoints>().Speed * timeSpeed;
            }

            yield return new WaitForSecondsRealtime(freezeTime);

            //string[] TagsToResumeSpeed =
            //{
            //    "OffDrag",
            //    "DragPoint"
            //};
            //foreach(string tag in TagsToResumeSpeed)
            //{
            //    allPoints = GameObject.FindGameObjectsWithTag(tag);
            //    foreach(GameObject point in allPoints)
            //    {
            //        point.GetComponent<MovingDragPoints>().Speed = point.GetComponent<MovingDragPoints>().Speed / timeSpeed;
            //    }
            //}

            if (Path == null)
            {
                //return;
            }

            pointPath = Path.GetNextPoint();
            pointPath.MoveNext();

            if (pointPath.Current == null)
            {
                //return;
            }

            transform.position = pointPath.Current.position;
            print("aaa");
        }
        
    }
}
