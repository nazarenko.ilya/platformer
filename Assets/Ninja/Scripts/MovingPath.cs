﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPath : MonoBehaviour
{
    public int movementDir = 1;
    public int movementTo = 0;
    public GameObject[] PathElements;
    public bool isTest = false;
    

    public void OnDrawGizmos()
    {
        if(PathElements == null || PathElements.Length < 2)
        {
            return;
        }

        for(var i = 1; i < PathElements.Length; i++)
        {
            Gizmos.DrawLine(PathElements[i - 1].transform.position, PathElements[i].transform.position);
        }

       
    }

    public IEnumerator<Transform> GetNextPoint()
    {
        if(PathElements == null || PathElements.Length < 1)
        {
            yield break;
        }
        while (true)
        {
            print("ADASDAS");
            yield return PathElements[movementTo].transform;

            if (PathElements.Length == 1)
            {
               continue;

                
            }
            if(movementTo <= 0)
            {
                movementDir = 1;
            }

            if(movementTo < PathElements.Length - 1)
            {
                movementTo = movementTo + movementDir;
                isTest = true;
            }
        }
    }
    public void Update()
    {
        PathElements = GameObject.FindGameObjectsWithTag("DragPoint");
    }

    public void Stop()
    {
        StopCoroutine(GetNextPoint());
    }
}
