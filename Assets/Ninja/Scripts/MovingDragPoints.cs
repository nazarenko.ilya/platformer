﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingDragPoints : MonoBehaviour, Imovable
{
    public float Speed = 10f;
    private Rigidbody rb;

    private Vector2 Moving;

    private void Start()
    {
        Moving = Vector3.right;
    }

    void FixedUpdate()
    {
        this.transform.Translate(Moving * Time.deltaTime * Speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("StopLine"))
        {
            FlipVector();
        }

    }

    void FlipVector()
    {
        Moving = -Moving;
    }
}
