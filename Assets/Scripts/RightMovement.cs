﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RightMovement : MonoBehaviour
{
    private Rigidbody rb;
    
    public float Impulse = 1000f;
    public float Gravity = 30f;

    public GameObject Handler;
    public GameObject Weapon;

    public GameObject Aimdot;
    public Transform Giz;

    public Image Stick;

    private bool isGrounded = false;
    private bool isPressed = false;
    public bool isShooting;
    private bool CanShoot;

    private GameMaster gm;

    public GameObject MainCanvasMain;
    public GameObject MainCanvasFinish;

    
    void Start()
    { 
        rb = GetComponent<Rigidbody>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        Time.timeScale = 1;

    }
    
    void FixedUpdate()
    {
        if (isGrounded)
        {
            if (isShooting)
            {
                Shooting();

            }
            if (!isShooting)
            {
                rb.AddForce(-transform.up * Gravity, ForceMode.Acceleration);
            }
        }
        if(!isGrounded)
        {
            if (isShooting)
            {
                Shooting();
            }

            if (!isShooting)
            {
                rb.AddForce(-transform.up * Gravity, ForceMode.Acceleration);
            }
        }
    }
    void Update()
    {
        Handler.transform.LookAt(Aimdot.transform.position);
        
        Aimdot.transform.position = (new Vector2(Stick.rectTransform.anchoredPosition.x / 30  + this.transform.position.x , Stick.rectTransform.anchoredPosition.y / 30  + this.transform.position.y));
        
        //RaycastHit hit;
        //if (Physics.Raycast(this.transform.position, -Vector3.up,out hit, 1.1f))
        //{
        //    if (hit.collider.CompareTag("Ground"))
        //    {
        //        isGrounded = true;
        //    }
        //}
        //else
        //{
        //    isGrounded = false;
        //}
        if(isGrounded && isPressed)
        {
            Death();
            print("Dead");
        }
        
    }

    private void LateUpdate()
    {
        RaycastHit _hit;
        Ray ray = new Ray(this.transform.position, Weapon.transform.forward);
        if(Physics.Raycast(ray, out _hit, Mathf.Infinity))
        {
            Giz.position = new Vector2(_hit.point.x, _hit.point.y);
        }
    }
    public void DownShootButton()
    {
        isShooting = true;
    }
    public void UpShootingButton()
    {
        isShooting = false;
    }
    private void Shooting()
    {
        rb.AddForce(-Weapon.transform.forward * Impulse, ForceMode.VelocityChange);
    }

    void Death()
    {
        transform.position = gm.LastCheckpointPos;
    }

    void Finish()
    {
        MainCanvasFinish.SetActive(true);
        MainCanvasMain.SetActive(false);
        Time.timeScale = 0;
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("MovingWall"))
        {
            isPressed = true;
        }
        if (other.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }

    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("MovingWall"))
        {
            isPressed = false;
        }
        if (other.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Death();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Finish"))
        {
            Finish();
        }
        
    }
}
