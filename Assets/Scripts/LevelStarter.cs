﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelStarter : MonoBehaviour
{
   public int LevelIndex;

   public void StartLevel()
   {
      SceneManager.LoadScene(LevelIndex);
   }
}
