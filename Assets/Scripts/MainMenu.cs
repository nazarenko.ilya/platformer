﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject MainWindow;

    public GameObject LevelsWindow;
    

    public void Back()
    {
        LevelsWindow.SetActive(false);
        MainWindow.SetActive(true);
    }

    public void LevelsOpen()
    {
        LevelsWindow.SetActive(true);
        MainWindow.SetActive(false);
    }
    
}

