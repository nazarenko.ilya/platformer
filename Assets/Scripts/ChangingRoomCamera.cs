﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangingRoomCamera : MonoBehaviour
{
    public GameObject RoomVirtualCam;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            RoomVirtualCam.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            RoomVirtualCam.SetActive(false);
            this.tag = "Enemy";
            this.GetComponent<Collider>().isTrigger = false;
        }
    }


}
