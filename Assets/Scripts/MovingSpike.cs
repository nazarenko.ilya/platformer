﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSpike : MonoBehaviour
{
    public float Speed = 10f;

    private Vector2 Moving;
    
    private void Start()
    {
        Moving = Vector2.right;
    }

    void FixedUpdate()
    {
        this.transform.Translate(Moving * Time.deltaTime * Speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("StopLine"))
        {
            FlipVector();
        }
        
    }

    void FlipVector()
    {
        Moving =-Moving;
    }
}
