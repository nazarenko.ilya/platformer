﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{
    public float Speed = 10f;

    private Vector2 Moving;

    public float Timer = 2f;

    public Collider StopLine1;
    public Collider StopLine2;
    
    private void Start()
    {
        Moving = Vector2.up * Speed;
    }

    void FixedUpdate()
    {
        this.transform.Translate(Moving * Time.deltaTime);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.CompareTag("StopLine"))
        //{
            //StartCoroutine(StartMoving());
        //}
        if (other == StopLine1)
        {
            StartCoroutine(StartMovingDown());
            print("aaaa");
        }
        if(other == StopLine2)
        {
            StartCoroutine(StartMovingUp());
            print("aaaa");
        }
        
    }

    IEnumerator StartMovingUp()
    {
        Moving = Vector2.zero;
        yield return new WaitForSecondsRealtime(Timer);
        Moving = Vector2.up * Speed;
    }

    IEnumerator StartMovingDown()
    {
        Moving = Vector2.zero;
        yield return new WaitForSecondsRealtime(Timer);
        Moving = Vector2.down * Speed;
    }
    
}
